**GenHash** est une interface graphique crée avec Python 3 et Gtk3+ (PyGI). Ce logiciel permet de générer des hash des fichiers en se basant sur le module hashlib de python 3.

#Installation 
Pour lancer le programme, tapez sur vos console : 
             ~$ python3 hash.py 
Veuillez noter que **GenHash** Gui dépend de python 3.


#Description 

**GenHash** est en cours d'évolution. 
**12/12/2014** 
**Fixed** - Support des hash suivants :
   * WEBM [Audio Basse Qualité]
   * SHA1
   * MD5
   * SHA256
   * SHA384
   * SHA512
   * OpenSSL
  
#TODO :
   - [ ]  Ajout des nouveaux algorithmes de hashage
   - [ ]  Hashage d'une entrée par l'utilisateur
   - [ ]  Ajouter des liens de documentation des algorithmes
   - [x]  Afficher la version de youtube-dl installée
   - [x]  Ajouter l'option de mettre à jour youtube-dl
   - [x]  Choix de la langue de l'interface dès son exécution 

#Bugs 
Veuillez notez les bugs sur https://github.com/Chiheb-Nexus/Youtube-dl_PyGtk_Gui

![alt tag](http://2.bp.blogspot.com/-lOQ907T6HR4/VGJrl8iqP-I/AAAAAAAAAwk/UjrrIEGQoRs/s1600/S%C3%A9lection_001.png)
![alt tag](http://4.bp.blogspot.com/-5PQqkKWkiAs/VHvvldhKvLI/AAAAAAAAA14/ubsbTNZmtNk/s1600/S%C3%A9lection_007.png)
